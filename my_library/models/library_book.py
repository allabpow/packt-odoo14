from odoo import models, fields


class LibraryBook(models.Model):
    _name = 'library.book'
    _description = 'A book for my_library'

    name = fields.Char('Title', required=True)
    date_release = fields.Date('Release Date')
    author_ids = fields.Many2many(
        'res.partner',
        string='Authors'
    )
