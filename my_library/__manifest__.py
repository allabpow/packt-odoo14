# -*- coding: utf-8 -*-

{
    'name': "My Library",
    'version': "0.0.1",
    'author': "Torsten Brieskorn",
    'depends': ["base"],
    'category': "Productivity",
    'summary': "Manage books easily",
    'description': """ 
         Manage Library
         Description related to library.
         """,
    'website': "http://bitkorn.de",
    'license': "LGPL-3",
    # This data files will be loaded at the installation (commented because file is not added in this example)
    # 'data': [
    #     'views.xml'
    # ],
    # This demo data files will be loaded if db initialize with demo data (commented because file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
