# packt-odoo14

This module is from the Packt> book [Odoo 14 Development Cookbook - Fourth Edition](https://www.packtpub.com/product/odoo-14-development-cookbook-fourth-edition/9781800200319)

Code files are under [github.com/PacktPublishing/Odoo-14-Development-Cookbook-Fourth-Edition](https://github.com/PacktPublishing/Odoo-14-Development-Cookbook-Fourth-Edition)

## Chapter 3: Creating Odoo Add-On Modules

Code files for this module under [github.com/PacktPublishing/Odoo-14-Development-Cookbook-Fourth-Edition/tree/master/Chapter03](https://github.com/PacktPublishing/Odoo-14-Development-Cookbook-Fourth-Edition/tree/master/Chapter03)